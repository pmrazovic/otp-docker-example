## 1. Clone repo

```
git clone https://bitbucket.org/pmrazovic/otp-docker-example
```

## 2. Build Docker image

```
docker build -t opentripplanner
```

## 3. Run container

```
docker run -p 8801:8801 opentripplanner
```

## 4. Open app

Open browser or send requests to [0.0.0.0:8801](http://0.0.0.0:8801)