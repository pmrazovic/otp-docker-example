FROM openjdk:8-jdk

COPY ./barcelona.gtfs.zip /opt/otp/
COPY ./barcelona.osm.pbf /opt/otp/
WORKDIR /opt/otp
RUN wget -O /opt/otp/otp.jar https://repo1.maven.org/maven2/org/opentripplanner/otp/1.3.0/otp-1.3.0-shaded.jar   

EXPOSE 8801

CMD java -Xmx2G -jar otp.jar --build /opt/otp --inMemory --port 8801